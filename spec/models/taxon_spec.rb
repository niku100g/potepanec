require "rails_helper"

RSpec.describe Spree::Taxon, type: :model do
  describe "#products_with_self_and_descendants" do
    subject { taxon.products_with_self_and_descendants }

    before do
      taxon_child.move_to_child_of(taxon)
      taxon_groundchild.move_to_child_of(taxon_child)
    end

    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { taxonomy.root }
    let!(:taxon_child) { create(:taxon, parent_id: taxon.id, taxonomy: taxonomy) }
    let!(:taxon_groundchild) { create(:taxon, parent_id: taxon.id, taxonomy: taxonomy) }
    let!(:another_taxon) { create(:taxon, parent_id: taxon.id, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_child) { create(:product, taxons: [taxon_child]) }
    let!(:product_groundchild) { create(:product, taxons: [taxon_groundchild]) }
    let!(:unrelated_product) { create(:product, taxons: [another_taxon]) }

    it "three products include product, product_child, product_groundchild" do
      is_expected.to contain_exactly(product, product_child, product_groundchild)
    end

    it "not include unrelated_product" do
      is_expected.not_to include unrelated_product
    end
  end
end
