require "rails_helper"

RSpec.feature "Products", type: :feature do
  describe "product_page" do
    given!(:category) { create(:taxonomy, name: 'Category') }
    given!(:bag) { create(:taxon, parent: category.root, name: 'BAG', taxonomy: category) }
    given!(:mug) { create(:taxon, parent: category.root, name: 'MUG', taxonomy: category) }
    given!(:panda_bag) { create(:product, name: "PANDA BAG", price: 19.99, taxons: [bag]) }
    given!(:panda_tote) { create(:product, name: "PANDA TOTE", taxons: [bag]) }
    given!(:panda_kaban) { create(:product, name: "PANDA KABAN", taxons: [bag]) }
    given!(:panda_mug) { create(:product, name: "PANDA MUG", taxons: [mug]) }

    background { visit potepan_product_path(panda_bag.id) }

    scenario "product_page shows self_product_details" do
      expect(page).to have_current_path(potepan_product_path(panda_bag.id))
      expect(page).to have_title(panda_bag.name + " | BIGBAG")
      expect(page).to have_selector ".media-body h2", text: panda_bag.name
      expect(page).to have_selector ".media-body h3", text: panda_bag.price
    end

    scenario "product_page includes related_products & works with related_products_link" do
      expect(page).to have_current_path(potepan_product_path(panda_bag.id))
      expect(page).to have_title(panda_bag.name + " | BIGBAG")
      click_on panda_tote.name
      expect(page).to have_current_path(potepan_product_path(panda_tote.id))
      expect(page).to have_title(panda_tote.name + " | BIGBAG")
      expect(page).to have_selector ".media-body h2", text: panda_tote.name
    end

    scenario "related_products not include self_product & another_product" do
      expect(page).to have_current_path(potepan_product_path(panda_bag.id))
      expect(page).to have_title(panda_bag.name + " | BIGBAG")
      expect(page).not_to have_selector ".productBox h5", text: panda_bag.name
      expect(page).not_to have_selector ".productBox h5", text: panda_mug.name
    end
  end
end
