require "rails_helper"

RSpec.feature "CategoriesShow", type: :feature do
  given!(:taxonomy) { create(:taxonomy) }
  given!(:taxon) { taxonomy.root }
  given!(:taxon_child) { create(:taxon, parent_id: taxon.id, taxonomy: taxonomy) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:product_child) { create(:product, taxons: [taxon_child]) }
  given!(:category) { create(:taxonomy, name: 'Category') }
  given!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
  given!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }
  given!(:panda_bag) { create(:product, name: 'Panda Bag', price: 19.99, taxons: [bag]) }
  given!(:lion_bag) { create(:product, name: 'Lion Bag', price: 29.99, taxons: [bag]) }
  given!(:tiger_mug) { create(:product, name: 'Tiger Mug', price: 39.99, taxons: [mug]) }

  background { visit potepan_category_path(taxon.id) }

  scenario "Display products of selected categories on the screen" do
    expect(page).to have_link product.name, href: potepan_product_path(product.id)
    expect(page).to have_link product_child.name, href: potepan_product_path(product_child.id)
    expect(page).to have_content product_child.display_price
    expect(page).to have_link(
      taxon_child.name, href: potepan_category_path(taxon_child.id)
    )
  end

  scenario "Transition from the category link to the product list page of the category" do
    click_link taxon_child.name
    expect(current_path).to eq potepan_category_path(taxon_child.id)
  end

  scenario "Transit from the product link to the detailed page of the product" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario 'Display products List/Grid' do
    within '.productGrid' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product_child.name
      expect(page).to have_content product_child.display_price
    end
  end
end
