class Potepan::HomeController < ApplicationController
  MAX_NEW_PRODUCTS_IN_PAGE = 10

  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::Search
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Pricing

  def index
    @new_products = Spree::Product.includes_image_and_price.new_arrivals.includes_variants.limit(MAX_NEW_PRODUCTS_IN_PAGE)
    @clothing = Spree::Taxon.where(name: "Clothing")
    @bags = Spree::Taxon.where(name: "Bags")
    @mugs = Spree::Taxon.where(name: "Mugs")
  end
end
