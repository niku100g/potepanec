Spree::Taxon.class_eval do
  scope :includes_variants, -> { includes(:variants) }

  def products_with_self_and_descendants
    scope = Spree::Product.includes(:taxons, master: [:images, :default_price]).includes_variants
    scope.where(
      spree_taxons: { id: self_and_descendants.pluck(:id) }
    )
  end
end
