require "rails_helper"

RSpec.describe "Products", type: :request do
  describe "#show" do
    let!(:product) { create(:product, product_properties: [product_property], taxons: [taxon]) }
    let!(:product_property) { create(:product_property) }
    let!(:taxon) { create(:taxon) }
    let!(:category) { create(:taxonomy, name: "Category") }
    let!(:bag) { create(:taxon, parent: category.root, name: "BAG", taxonomy: category) }
    let!(:mug) { create(:taxon, parent: category.root, name: "MUG", taxonomy: category) }
    let!(:panda_mug) { create(:product, name: "PANDA MUG", taxons: [mug]) }

    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully" do
      expect(response).to have_http_status(:ok)
    end

    it "renders show template" do
      expect(response).to render_template(:show)
    end

    it "assigns product" do
      expect(assigns(:product)).to eq product
    end

    it "assigns product_properties" do
      expect(assigns(:product_properties)).to eq [product_property]
    end
  end

  describe "#related_products" do
    subject { assigns(:related_products) }

    let!(:product) { create(:product, product_properties: [product_property], taxons: [taxon]) }
    let!(:product_property) { create(:product_property) }
    let!(:taxon) { create(:taxon) }
    let!(:category) { create(:taxonomy, name: "Category") }
    let!(:bag) { create(:taxon, parent: category.root, name: "BAG", taxonomy: category) }
    let!(:mug) { create(:taxon, parent: category.root, name: "MUG", taxonomy: category) }
    let!(:panda_mug) { create(:product, name: "PANDA MUG", taxons: [mug]) }

    before do
      get potepan_product_path(product.id)
    end

    context "When there are three related products" do
      let!(:three_related_products) { create_list(:product, 3, taxons: [taxon]) }

      it { expect(subject.count).to eq 3 }
    end

    context "When there are four_related_products" do
      let!(:four_related_products) { create_list(:product, 4, taxons: [taxon]) }

      it { expect(subject.count).to eq 4 }
    end

    context "When there are five_related_products" do
      let!(:five_related_products) { create_list(:product, 5, taxons: [taxon]) }

      it { expect(subject.count).to eq 4 }
    end

    context "When the item was a bag" do
      let!(:four_related_products) { create_list(:product, 4, taxons: [bag]) }

      it { expect(subject).not_to include panda_mug }
    end
  end
end
