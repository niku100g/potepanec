class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products_with_self_and_descendants.
      with_color(params[:color]).with_size(params[:size]).
      filter_by_option(option_value: params[:option_value]).
      order_by(params[:order])
    @taxonomies = Spree::Taxonomy.includes(:root)
    @list_of_colors = Spree::OptionValue.with_color_and_products_in(@products.ids)
    @list_of_sizes = Spree::OptionValue.with_size_and_products_in(@products.ids)
  end
end
