module CategoriesHelper
  def grid_layout?
    !list_layout?
  end

  def list_layout?
    params[:layout] == "list"
  end
end
