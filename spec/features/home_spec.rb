require "rails_helper"

RSpec.feature "Home", type: :feature do
  given!(:taxonomy) { create(:taxonomy) }
  given!(:taxon) { taxonomy.root }
  given!(:new_product) { create(:product, taxons: [taxon]) }
  given!(:category) { create(:taxonomy, name: 'Category') }
  given!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
  given!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }

  background { visit potepan_category_path(taxon.id) }

  scenario "Display products of selected categories on the screen" do
    expect(page).to have_link new_product.name, href: potepan_product_path(new_product.id)
    expect(page).to have_content new_product.display_price
  end

  scenario "Transit from the product link to the detailed page of the product" do
    click_link new_product.name
    expect(current_path).to eq potepan_product_path(new_product.id)
  end
end
