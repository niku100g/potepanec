Spree::OptionValue.class_eval do
  scope :with_color_and_products_in, -> (product_ids) do
    joins(:option_type, :variants).
      where("spree_option_types.name = ?", "tshirt-color").
      where("spree_variants.product_id IN (?)", product_ids).
      distinct
  end

  scope :with_size_and_products_in, ->(product_ids) do
    joins(:option_type, :variants).
      where("spree_option_types.name = ?", "tshirt-size").
      where("spree_variants.product_id IN (?)", product_ids).
      distinct
  end
end
