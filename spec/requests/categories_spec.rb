require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "#show" do
    let!(:category) { create(:taxonomy, name: 'Category') }
    let!(:brand) { create(:taxonomy, name: 'Brand') }

    let!(:bag) { create(:taxon, parent: category.root, name: 'Bag', taxonomy: category) }
    let!(:mug) { create(:taxon, parent: category.root, name: 'Mug', taxonomy: category) }

    let!(:panda_bag) { create(:product, name: 'Panda Bag', price: 19.99, taxons: [bag]) }
    let!(:lion_bag) { create(:product, name: 'Lion Bag', price: 29.99, taxons: [bag]) }
    let!(:tiger_mug) { create(:product, name: 'Tiger Mug', price: 39.99, taxons: [mug]) }

    before do
      get potepan_category_path(bag.id)
    end

    it "responds successfully" do
      expect(response).to have_http_status(:ok)
    end

    it "renders show template" do
      expect(response).to render_template(:show)
    end

    it "assigns all taxonomy to @taxonomies" do
      expect(assigns(:taxonomies)).to contain_exactly(category, brand)
    end

    it "assigns the requested taxon to @taxon" do
      expect(assigns(:taxon)).to eq bag
    end

    it "assigns the requested products to @products" do
      expect(assigns(:products)).to contain_exactly(panda_bag, lion_bag)
    end

    it "displays related products & not unrelated products" do
      expect(response.body).to include panda_bag.name
      expect(response.body).to include "#{panda_bag.display_price}"
      expect(response.body).to include lion_bag.name
      expect(response.body).to include "#{lion_bag.display_price}"
      expect(response.body).not_to include tiger_mug.name
      expect(response.body).not_to include "#{tiger_mug.display_price}"
    end
  end
end
