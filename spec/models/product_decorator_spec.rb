require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  describe "scope" do
    describe "without_product" do
      subject { Spree::Product.without_product(product) }

      let!(:taxonomy) { create(:taxonomy) }
      let!(:taxonomy2) { create(:taxonomy) }
      let!(:product) { create(:product, taxons: [taxonomy.root]) }
      let!(:related_product) { create(:product, taxons: [taxonomy.root]) }
      let!(:unrelated_product) { create(:product, taxons: [taxonomy2.root]) }

      it { is_expected.not_to include product }

      it { is_expected.to contain_exactly(related_product, unrelated_product) }
    end
  end

  describe "#related_products" do
    subject { product.related_products }

    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxonomy2) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxonomy.root]) }
    let!(:related_product) { create(:product, taxons: [taxonomy.root]) }

    it { is_expected.to contain_exactly related_product }
  end
end
