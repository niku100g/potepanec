require 'rails_helper'

RSpec.describe "Home", type: :request do
  describe "#index" do
    subject { get :index }

    let(:two_year_old_product) { create(:product, available_on: 2.year.ago) }
    let(:one_year_old_product) { create(:product, available_on: 1.year.ago) }

    before do
      get potepan_root_path
    end

    it "responds successfully" do
      expect(response.status).to eq 500
    end

    it "renders show template" do
      expect(response).to render_template(:index)
    end

    it "new_products have products.order(available_on: :desc)" do
      expect(assigns(:new_products)).to match([one_year_old_product, two_year_old_product])
    end

    context "when there are so many products available" do
      let(:max_new_products_in_page) { 10 }
      let!(:products) { create_list(:product, 11) }

      it "@new_products.to_a.size  MAX_NEW_PRODUCTS_IN_PAGE" do
        expect(assigns(:new_products).to_a.size).to be max_new_products_in_page
      end
    end
  end
end
