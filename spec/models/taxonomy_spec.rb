require "rails_helper"

RSpec.describe Spree::Taxonomy, type: :model do
  describe "#root_taxons" do
    subject { taxonomy.root_taxons }

    before do
      taxonomy.root = root_taxon
      taxonomy.taxons = [root_taxon, child_taxon]
    end

    let!(:taxonomy) { create(:taxonomy) }
    let!(:root_taxon) { taxonomy.root }
    let!(:child_taxon) { create(:taxon, parent_id: root_taxon.id, taxonomy: taxonomy) }

    it "include child_taxon" do
      is_expected.to include child_taxon
    end

    it "not include root_taxon" do
      is_expected.not_to include root_taxon
    end
  end
end
