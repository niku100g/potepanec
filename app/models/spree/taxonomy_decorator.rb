Spree::Taxonomy.class_eval do
  def root_taxons
    taxons.where(parent_id: root.id)
  end
end
