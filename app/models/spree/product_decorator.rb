Spree::Product.class_eval do
  MAX_PRODUCTS_TO_FETCH = 20

  scope :new_arrivals, -> { order(available_on: :desc) }
  scope :without_product, ->(product) { where.not(id: product.id) }
  scope :includes_image_and_price, -> { includes(master: [:images, :default_price]) }
  scope :includes_variants, -> { includes(:variants) }

  scope :filter_by_option, -> (option) {
    if option[:option_value].present?
      joins(variants: :option_values).
        where(spree_option_values: { name: option[:option_value] })
    end
  }

  add_search_scope :with_color do |color|
    if color.nil?
      all
    else
      includes(variants_including_master: :option_values).
        where("spree_option_values.name = ? AND spree_option_values.option_type_id = ?",
              color, Spree::OptionType.find_by(name: "tshirt-color")).references(:option_values)
    end
  end

  add_search_scope :with_size do |size|
    if size.nil?
      all
    else
      includes(variants_including_master: :option_values).
        where("spree_option_values.name = ? AND spree_option_values.option_type_id = ?",
              size, Spree::OptionType.find_by(name: "tshirt-size")).references(:option_values)
    end
  end

  def related_products
    self.class.joins(:taxons).
      includes_image_and_price.
      includes_variants.
      where(spree_taxons: { id: taxons.ids }).
      without_product(self).
      distinct.
      limit(MAX_PRODUCTS_TO_FETCH)
  end

  scope :order_descend_by_price, -> { joins(:prices).order("spree_prices.amount DESC") }
  scope :order_ascend_by_price, -> { joins(:prices).order("spree_prices.amount ASC") }
  scope :order_newest_by_product_available_on, -> { joins(:taxons).order("spree_products.available_on DESC") }
  scope :order_oldest_by_product_available_on, -> { joins(:taxons).order("spree_products.available_on ASC") }
  scope :order_by, ->(order) {
    case order
    when 'price-dsc' then order_descend_by_price
    when 'price-asc' then order_ascend_by_price
    when 'oldest'    then order_oldest_by_product_available_on
    else order_newest_by_product_available_on
    end
  }

  def self.order_option_hash
    {
      "新着順":      'newest',
      "価格の安い順": 'price-asc',
      "価格の高い順": 'price-dsc',
      "古い順":      'oldest',
    }
  end
end
